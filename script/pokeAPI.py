import requests
import json

# Lists
pokemons = ["aipom","lakazam","pikachu","bulbasaur","celebi"]
pokemon_spec_list = []

# Retrieving Pokemon data from get api call
for pokemon in pokemons:
    url = "https://pokeapi.co/api/v2/pokemon/" + pokemon
    
    try:
        response = requests.get(url)
        if (response.status_code != 200):
            raise AssertionError(response.status_code) 
        else:                 
            poke_name = response.json()['name']
            poke_xp = response.json()['base_experience']
            
            pokemon_spec_list.append({"name":poke_name, "base_experience":poke_xp})
        
    except:
        if (response.status_code == 404):
            print(" == /!\ == No information about this pokemon. Are you sure it exists ?  => Not Found :", response.status_code)
        else:
            print("KO. Status code => ", response.status_code)

#  Display List
print(pokemon_spec_list)

# Dump List into json format and write data in file
try :
    with open('pokeList.json', 'w', encoding='utf-8') as f:
        json.dump(pokemon_spec_list, f, ensure_ascii=False, indent=4) 
except :
    print("dumping list to json has failed")     

 
       

    


