FROM python:3.10-alpine

ADD script/pokeAPI.py .

RUN pip install requests

CMD ["python", "./pokeAPI.py"]